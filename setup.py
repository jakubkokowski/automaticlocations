from setuptools import setup

setup(
    name='AutomaticLocationsTools',
    version='0.0.1',
    packages=['AutomaticLocationsTools'],
    url='https://bitbucket.org/jakubkokowski/automaticlocations/src',
    license='GNU GENERAL PUBLIC LICENSE',
    author='Jakub Kokowski',
    author_email='jakubkokowski@gmail.com',
    description='Program is a support for BackTrackBB software. It uses ObsPy Coincidence Trigger to detect and save '
                'separate mseed files.'
)
