# Automatic Locations Tools
Program uses ObsPy Coincidence Trigger for event detections. Triggered events are 
saved to separate mseed files which are the input for BackTrackBB automatic 
location program.

Processing flow:

1. Automatic detections: saving txt and xls files with detection results, as well as with short 
mseed files and BackTrackBB configuration files.
2. Automatic locations with BackTrackBB: saving all BTBB files as well as xlx one day catalogs.
3. Relocation with BTBB for shorter time window basing on obtained origin time from the first location.
4. Optionally: generation of a full catalog containing relocation, location or detection results.

## Installation

1. Create directory and clone repository.
```sh
git clone https://jakubkokowski@bitbucket.org/jakubkokowski/automaticlocations.git
cd automaticlocations
```
2. Use environment.yml file to create conda environment.
```sh
conda env create -f environment.yml
conda activate alt_2022
```
3. Use pip to install the AutomaticLocationsTools package.
```sh
pip install .
```
4. Install [BackTrackBB](https://github.com/BackTrackBB/backtrackbb) - clone 
or download project from GitHub. Inside the downloaded directory run:
```sh
pip install .
```
Conda's environment *alt_2022* has to be active.

5. (Optionally) if you notice a WARNING after installing BackTrackBB : *pkgresourcesdeprecationwarning: 6030-dirty is an invalid version and will not 
be supported in a future release* you can run:
```sh
conda update setuptools
```
6. (Optionally) install NonLinLoc, which is used for creating travel-time tables.



## Usage
1. Prepare *config.py* and BTBB.conf files according to your needs.
2. Create timetables (NonLinLoc 3D format- can be generated using [NLL](http://alomax.free.fr/nlloc/)),
name the directory "time_btbb" and move it to *your_dir* directory.
3. Make sure that timetables were created for all seismic stations and put their names into config.py file.

## Detailed documentation
[Documentation](https://bitbucket.org/jakubkokowski/automaticlocations/wiki/Home)

## Examples
### Multiple day location

```python
from AutomaticLocationsTools import Alt

# Using program for data range: the first and the last day for which the program is used
start_date = Alt.datetime(year=2022, month=1, day=1)
end_date = Alt.datetime(year=2022, month=1, day=30)
Alt.full_btbb_multiple_day_location_procedure(start_date, end_date)
```
### Yesterday location
Can be used as a daily routine with crontab.

```python
from AutomaticLocationsTools import Alt

Alt.yesterday_full_location_procedure()
```