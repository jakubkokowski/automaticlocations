import datetime

from AutomaticLocationsTools import Alt

# Alf.yesterday_full_location_procedure()
start_date = Alt.datetime(year=2021, month=12, day=1)
end_date = Alt.datetime(year=2021, month=12, day=31)
print(datetime.datetime.utcnow())
#Alt.full_btbb_multiple_day_location_procedure(start_date, end_date)
Alt.automatic_detection(start_date, detection_output=True, btbb_files=True)
print(datetime.datetime.utcnow())
