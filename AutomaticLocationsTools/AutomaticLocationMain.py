#  Automatic Locations Flow: main functions
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from datetime import datetime

from AutomaticLocationsTools.AutomaticDetector import automatic_detection, date_range, create_relocation_btbb_configs, yesterday, check_if_detections_exist
from AutomaticLocationsTools.AutomaticLocationsProcessing import to_one_catalog, prepare_mseeds, compare_stations_in_configs
from AutomaticLocationsTools.BTBBFlow import btbb_automatic_locations


def full_btbb_one_day_location_procedure(date: datetime):
    """ Function runs full BackTrackBB location procedure for one day.
     1. Detection
     2. Location
     3. Relocation - location for a smaller time window.

    Automatic detections are not calculated once again if an output directory for detections for a given day
    is not empty.

    :param date: datetime
    :return:
    """

    print("##########################################")
    print("Full one day location procedure for:")
    print(date.strftime('%d-%m-%Y'))
    if not check_if_detections_exist(date):
        automatic_detection(date, detection_output=True, btbb_files=True)
    btbb_automatic_locations(date, btbb=True, quakeml=False, catalogs=True, relocation=False)
    # Prepare BTBB config files for shorter time window
    # flag for output name: _out (first location), _out2 (relocation), _out3 (second relocation) etc.
    nr = '2'
    create_relocation_btbb_configs(date, nr=nr)
    btbb_automatic_locations(date, btbb=True, quakeml=True, catalogs=True, relocation=True, nr=nr)


def full_btbb_multiple_day_location_procedure(startdate: datetime, enddate: datetime):
    """ Function runs full_btbb_one_day_location_procedure function for given range of dates.

    :param startdate: datetime
    :param enddate: datetime
    :return:
    """

    compare_stations_in_configs()
    for date in date_range(startdate, enddate):
        full_btbb_one_day_location_procedure(date)


def yesterday_full_location_procedure():
    """ Function runs full_btbb_one_day_location_procedure function for yesterday.

    :return:
    """

    compare_stations_in_configs()
    date = yesterday()
    full_btbb_one_day_location_procedure(date)


def generate_catalog(startdate: datetime, enddate: datetime):
    """Function merge xlsx output files for final (relocation) results for given range of dates.

    :param startdate: datetime
    :param enddate: datetime
    :return:
    """

    to_one_catalog(startdate, enddate, det_or_loc='relocation')


if __name__ == "__main__":
    # range of dates for which the program is used
    start_date = datetime(year=2022, month=1, day=1)
    end_date = datetime(year=2022, month=1, day=2)
    full_btbb_multiple_day_location_procedure(start_date, end_date)
