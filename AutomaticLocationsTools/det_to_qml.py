# Writing detection to QuakeML
# ver. 2022.03.09

#
from obspy.core.event.base import Comment
from obspy.core.event import Catalog
from obspy.core.event import Event
from obspy.core.event import Pick
from obspy.core.event import Origin
from obspy.core.event import OriginQuality
from obspy import UTCDateTime
from obspy.core.event.base import WaveformStreamID


ppos = ['net', 'sta', 'channel', 'name', 'year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']


class AutoCatalog:
    """AutoCatalog is an obspy.core.event.Catalog.

    Depth is stored in kilometers. Depth is operator assigned.
    """

    def __init__(self, region_name):
        self.region = region_name
        self.catalog = Catalog(resource_id="smi:igf.edu.pl/{}#eventParameters".format(region_name.replace(' ', '_')))

    def add_detection(self, your_picks: list, your_location: dict, loc_algorithm: str):
        """Add detection to ObsPy.Catalog

        :param loc_algorithm: location algorithm
        :param your_picks: list of picks (each pick is a list of parameters: )
        :param your_location: dict
        :return:
        """

        event = Event()
        for p in your_picks:
            pick = Pick()
            pick.time = UTCDateTime(p['year'], p['month'], p['day'], p['hour'], p['minute'], p['second'],
                                    p['microsecond'])  # integer
            pick.waveform_id = WaveformStreamID(network_code=p['net'], station_code=p['sta'], channel_code=p['channel'])
            pick.phase_hint = p['name']  # string
            pick.evaluation_mode = 'automatic'
            pick.evaluation_status = 'preliminary'
            event.picks.append(pick)
        origin = Origin()
        origin.time = UTCDateTime(your_location['year'], your_location['month'], your_location['day'],
                                  your_location['hour'], your_location['minute'], your_location['second'],
                                  your_location['microsecond'])
        origin.latitude = your_location['latitude']  # float
        origin.longitude = your_location['longitude']  # float
        origin.depth = your_location['depth'] / 1000.0  # float in kilometers (SWIP5 origin version) down the see level
        origin.depth_type = 'operator assigned'
        origin.region = self.region
        origin.evaluation_mode = "automatic"
        origin.evaluation_status = 'preliminary'
        origin.quality = OriginQuality(used_phase_count=len(your_picks))
        origin.extra={'algorithm': {'value': loc_algorithm,
                                     'namespace': 'igf'}}
        event.origins.append(origin)

        self.catalog.append(event)

    def write(self, file_name: str):
        """Save catalog in quakeml format.

        :param file_name: str
        :return:
        """

        self.catalog.write(file_name, format='QUAKEML')

    def clear(self):
        """Clear catalog.

        :return:
        """

        self.catalog.clear()


# Test
if __name__ == '__main__':
    cat = AutoCatalog('Lumineos')
    pp = [
        ['PL', 'SS1', 'EHZ', 'P', 2000, 1, 1, 0, 1, 0, 1],
        ['PL', 'SS2', 'EHZ', 'P', 2000, 1, 1, 0, 1, 1, 10],
        ['PL', 'SS3', 'EHZ', 'P', 2000, 1, 1, 0, 1, 2, 100],
        ['PL', 'SS4', 'EHZ', 'P', 2000, 1, 1, 0, 1, 2, 1000],
        ['PL', 'SS5', 'EHZ', 'P', 2000, 1, 1, 0, 1, 3, 2000],
        ['PL', 'SS6', 'EHZ', 'P', 2000, 1, 1, 0, 1, 1, 10000],
        ['PL', 'SS7', 'EHZ', 'P', 2000, 1, 1, 0, 1, 2, 20000],
        ['PL', 'SS8', 'EHZ', 'P', 2000, 1, 1, 0, 1, 3, 100000],
        ['PL', 'SS9', 'EHZ', 'P', 2000, 1, 1, 0, 1, 1, 123456],
    ]
    ppos = ['net', 'sta', 'channel', 'name', 'year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']
    picks = [dict(zip(ppos, p)) for p in pp]
    print("Test {} picks:".format(len(picks)))
    for p in picks:
        print("  {}".format(p))
    print("Test location:")
    location = {'year': 2000, 'month': 1, 'day': 1, 'hour': 23, 'minute': 59, 'second': 59, 'microsecond': 10,
                'latitude': 12.3, 'longitude': 123.4, 'depth': 12345.6}
    print("  {}".format(location))
    cat.add_detection(picks, location)
    output_file = 'testqml.xml'
    print("Output file:\n   {}".format(output_file))
    cat.write(output_file)
