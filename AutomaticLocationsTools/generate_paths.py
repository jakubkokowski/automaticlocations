import os
import datetime

import config


def create_dir_if_not_exist(path_out: str):
    """Function checks whether the directory exist

    :param path_out: str
    :return:
    """

    if not os.path.exists(path_out):
        os.makedirs(path_out)


def day_path(date: datetime) -> str:
    """Function returns path to detection and location results for an individual day.

    :param date:
    :return: str, path
    """

    '''
    year, day_of_year = year_and_day_info(date)
    date_string = year_and_day_to_string(year, day_of_year)
    '''
    day_path = os.path.join(config.path_out, date.strftime("%d_%m_%Y"))
    create_dir_if_not_exist(day_path)
    return day_path


def detector_out_path(date: datetime) -> str:
    """Function returns path to detection results.

    :param date: datetime
    :return: str, path
    """
    day_path_detection = os.path.join(day_path(date), 'Detections')
    create_dir_if_not_exist(day_path_detection)
    return day_path_detection


def detector_waveforms_out_path(date: datetime) -> str:
    """Function returns path to detection results.

    :param date: datetime
    :return: str, path
    """
    day_path_detection = os.path.join(day_path(date), 'Detections', 'Event_waveforms')
    create_dir_if_not_exist(day_path_detection)
    return day_path_detection


def btbb_out_path(date: datetime) -> str:
    """Function returns path to BackTrackBB location results.

    :param date: datetime
    :return: str, path
    """
    day_path_btbb = os.path.join(day_path(date), 'BTBB', 'Locations')
    create_dir_if_not_exist(day_path_btbb)
    return day_path_btbb


def btbb_relocation_out_path(date: datetime, nr='2') -> str:
    """Function returns path to BackTrackBB relocation results.

    :param date: datetime
    :param nr: str label
    :return: str, path
    """
    day_path_btbb = os.path.join(day_path(date), 'BTBB', 'Locations_'+nr)
    create_dir_if_not_exist(day_path_btbb)
    return day_path_btbb


def nll_path(date: datetime) -> str:
    """Function returns path to NLL location results.

    :param date: datetime
    :return: str, path
    """
    day_path_nll = os.path.join(day_path(date), 'NLL')
    create_dir_if_not_exist(day_path_nll)
    return day_path_nll


def nll_inputs_path_p(date: datetime) -> str:
    """Function returns path to NLL location results.

    :param date: datetime
    :return: str, path
    """
    day_path_nll = os.path.join(day_path(date), 'NLL', 'Input_files_P')
    create_dir_if_not_exist(day_path_nll)
    return day_path_nll


def nll_inputs_path_ps(date: datetime) -> str:
    """Function returns path to NLL location results.

    :param date: datetime
    :return: str, path
    """
    day_path_nll = os.path.join(day_path(date), 'NLL', 'Input_files_PS')
    create_dir_if_not_exist(day_path_nll)
    return day_path_nll


def nll_outputs_path_p(date: datetime) -> str:
    """Function returns path to NLL location results.

    :param date: datetime
    :return: str, path
    """
    day_path_nll = os.path.join(day_path(date), 'NLL', 'Locations_P')
    create_dir_if_not_exist(day_path_nll)
    return day_path_nll


def nll_outputs_path_ps(date: datetime) -> str:
    """Function returns path to NLL location results.

    :param date: datetime
    :return: str, path
    """
    day_path_nll = os.path.join(day_path(date), 'NLL', 'Locations_PS')
    create_dir_if_not_exist(day_path_nll)
    return day_path_nll
