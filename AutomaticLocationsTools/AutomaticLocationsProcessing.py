#  Automatic Location Processing
#
#  Additional functions for post processing of location and detection results
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from datetime import datetime
import os
import shutil

import pandas as pd

from AutomaticLocationsTools.generate_paths import nll_path, btbb_out_path, btbb_relocation_out_path, day_path,  \
     create_dir_if_not_exist, nll_outputs_path_ps, nll_outputs_path_p, detector_out_path
from AutomaticLocationsTools.AutomaticDetector import prepare_list_of_day_events,date_range
from AutomaticLocationsTools.BTBBFlow import read_btbb_output, check_if_btbb_file_exist_and_isnot_empty
# importing configuration file, which should be edited by users
import config


def compare_stations_in_configs():
    """ Function check compatibility of station lists between two configuration files: config.py and BTBB.conf.

    :return: None
    """
    with open(config.BTBB_conf_file_base, 'r') as f:
        lines = f.readlines()
        for i, line in enumerate(lines):
            if 'stations' in line:
                stations = line.split('=')[-1]
    stations_btbb=stations.replace(' ','')
    stations_btbb=stations_btbb.replace('\n','')
    stations_btbb = sorted(stations_btbb.split(','))
    stations_detector = sorted(config.stations)
    print(80*'#')
    print('Checking stations in config files')
    print('config.py stations: ', stations_detector)
    print('btbb.conf stations: ', stations_btbb)
    notinbtbb = [x for x in stations_detector if x not in stations_btbb]
    notinconfigpy = [x for x in stations_btbb if x not in stations_detector]
    if notinconfigpy or notinbtbb:
        print('WARNING')
        print('Stations not included in BTBB.conf file: ', notinbtbb)
        print('Stations not included in config.py file: ', notinconfigpy)
    print(80*'#')


def to_one_catalog(start_date: datetime, end_date: datetime, det_or_loc='detection', nr='2'):
    """ Function merge output xlsx files into one catalog xlsx file for given range of dates.

    :param start_date:
    :param end_date:
    :param det_or_loc: str - 'detection' (files from detection directories), 'nll_p', 'nll_ps',
     'location' (files from location directories) or 'relocation' (files from relocation directories)
    :param nr: str, optional - label for relocation directories ('2' for normal, first relocation)
    :return:
    """

    print("##########################################")
    print("Start date: ", start_date.strftime('%d-%m-%Y'))
    print("End date: ", end_date.strftime('%d-%m-%Y'))

    cat = pd.DataFrame()
    if det_or_loc == 'location':
        print("Full location catalog")
    elif det_or_loc == 'detection':
        print("Full detection catalog")
    elif det_or_loc == 'relocation':
        print("Full relocation catalog")
    elif det_or_loc == 'nll_p':
        print("Full NonLinLoc catalog (P inputs only)")
    elif det_or_loc == 'nll_ps':
        print("Full NonLinLoc catalog (P and S inputs)")
    else:
        print('Wrong det_or_loc parameter')
        return None
    for date in date_range(start_date, end_date):
        if det_or_loc == 'location':
            path = os.path.join(btbb_out_path(date), config.final_xlsx_file)
        elif det_or_loc == 'detection':
            path = os.path.join(detector_out_path(date), config.detector_xlsx_file)
        elif det_or_loc == 'relocation':
            path = os.path.join(btbb_relocation_out_path(date, nr), config.final_xlsx_file)
        elif det_or_loc == 'nll_p':
            path = os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_p.xlsx")
        elif det_or_loc == 'nll_ps':
            path = os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_ps.xlsx")
        else:
            break
        if os.path.isfile(path):
            data = pd.read_excel(path, index_col='Nr')
            cat = cat.append(data)
            print("File read for: ", date.strftime('%d-%m-%Y'))
        else:
            print("No file for: ", date.strftime('%d-%m-%Y'))
    cat_path = os.path.join(config.path_out, f"{det_or_loc}_catalog_{start_date.strftime('%d_%m_%Y')}_"
                                             f"{end_date.strftime('%d_%m_%Y')}.xlsx")
    cat.reset_index()
    cat.to_excel(cat_path)
    print("Catalog saved to: ", cat_path)
    print("##########################################")


def full_catalog():
    # polaczenie
    return 0

def prepare_mseeds(start_date: datetime, end_date: datetime, nr='2'):
    """Function copy all mseed files into one directory and create xlsx file containing events details.

    :param start_date:
    :param end_date:
    :param nr: str, optional - label for relocation directories ('2' for normal, first relocation)
    :return:
    """

    mseed_dir = 'mseeds'
    create_dir_if_not_exist(mseed_dir)
    mseed_cat = 'mseed_catalog.xlsx'
    df = pd.DataFrame()
    for date in date_range(start_date, end_date):
        day_path2 = day_path(date)
        if os.path.isdir(day_path2):
            for root, dirs, files in prepare_list_of_day_events(day_path2):
                for name in dirs:
                    event_path = os.path.join(btbb_relocation_out_path(date,nr), name)
                    if check_if_btbb_file_exist_and_isnot_empty(event_path):
                        trig_nr = str(event_path).split('_')[-1]
                        time, lat, lon, rms, maxst=read_btbb_output(event_path)
                        datetime_object = datetime.strptime(time, '%Y-%m-%dT%H:%M:%S.%fZ')
                        mseed_name = datetime_object.strftime('%Y%m%d%H%M%S') + '.mseed'
                        location = {'trig_nr': trig_nr, 'date': datetime_object, 'latitude': float(lat),
                                    'longitude': float(lon), 'MaxStack': float(maxst), 'mseed_name': mseed_name}
                        if location:
                            mseed_file = os.path.join(day_path2,'trig_'+trig_nr, config.mseed_name)
                            if os.path.isfile(mseed_file):
                                df = df.append(location, ignore_index=True)
                                shutil.copyfile(mseed_file, os.path.join(mseed_dir, mseed_name))
    df.to_excel(os.path.join(mseed_dir, mseed_cat))


#######################################################


if __name__ == "__main__":
    os.chdir('..')
    compare_stations_in_configs()