import datetime
import os

import obspy
import numpy as np
import pandas as pd

from AutomaticLocationsTools.generate_paths import *
from AutomaticLocationsTools.det_to_qml import AutoCatalog
import config


def quakeml_parser(quakeml):
    cat = obspy.read_events(quakeml, format="QUAKEML")
    events=[]
    for event in cat:
        if len(event.origins) > 0:
            origin=event.origins[0]
            lon = origin.longitude
            lat = origin.latitude
            time = origin.time
            picks = []
            try:
                picks = event.picks
            except:
                print("No picks for the event")
            event_dict = {"time": time, "latitude": lat, "longitude": lon, "picks": picks}
            events.append(event_dict)
    return events


def create_nll_input_files(quakeml, date, only_p=True):
    events = quakeml_parser(quakeml)
    if only_p:
        directory_out = nll_inputs_path_p(date)
    else:
        directory_out = nll_inputs_path_ps(date)
    for event in events:
        name = str(event["time"]).replace(":", "").replace("-", "").replace("T", "")[2:-8] + ".txt"
        with open(os.path.join(directory_out, name), "a+") as f:
            for p in event["picks"]:
                line = p['waveform_id']['station_code']
                line = append_line(line, 7)
                line = line + "?"
                line = append_line(line, 12)
                line = line + "?"
                line = append_line(line, 17)
                line = line + "? "
                line = line + p["phase_hint"] + "      ? "
                p_pick_time = str(p["time"]).replace(":", "").replace("-", "").replace("T", "")
                line = line + p_pick_time[:8] + " "
                line = line + p_pick_time[8:12] + "   "
                line = line + p_pick_time[12:19] + " "
                line = line + " GAU  2.00e-02 -1.00e+00 -1.00e+00 -1.00e+00"
                if only_p and p["phase_hint"] == 'P' or not only_p:
                    f.write(line)
                    f.write("\n")


def prepare_nll_config(date: datetime.datetime, only_p=True):
    if only_p:
        path_out = nll_outputs_path_p(date)
        path_in = nll_inputs_path_p(date)
    else:
        path_out = nll_outputs_path_ps(date)
        path_in = nll_inputs_path_ps(date)
    with open(config.NLL_conf_file, 'r') as f:
        save_path = os.path.join(path_in, "nll_config.in")
        file_out = open(save_path, "w")
        lines = f.readlines()
        for i, line in enumerate(lines):
            if 'INPUT_PATH' in line:
                path = os.path.join(path_in, '*.txt')
                line = f"LOCFILES {path} NLLOC_OBS  ./time/layer  {path_out}/loc\n"

            file_out.write(line)
        file_out.close()
    return save_path


def append_line(line, number):
    line = line+" "*(number-len(line))
    return line


def run_nll(path_to_nll_config):
    print("##########################################")
    try:
        print("start of NLL")
        os.system(f"NLLoc {path_to_nll_config}")
        print("end of NLL")
    except:
        print("!!! Locations with NLL unsuccessful !!!")
    print("##########################################")


def xls_from_nll(date, only_p=True):
    if only_p:
        path = os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_p.xml")
    else:
        path = os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_ps.xml")
    if os.path.exists(path):
        events = quakeml_parser(path)
        lst_events = []
        j = 1
        for event in events:
            #number_of_picks = len(event['picks'])
            lst_events.append([j, event['time'], event['latitude'], event['longitude']])
            j += 1
        df = pd.DataFrame(lst_events, columns=['Nr', 'Date', 'Lat', 'Lon'])
        df = df.set_index('Nr')
        if only_p:
            df.to_excel(os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_p.xlsx"))
        else:
            df.to_excel(os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_ps.xlsx"))


def quakeml_from_nll(date, only_p=True):
    if only_p:
        nll_results = nll_outputs_path_p(date)
        loc_algorithm = "NLL_P"
    else:
        nll_results = nll_outputs_path_ps(date)
        loc_algorithm = "NLL_PS"

    cat = AutoCatalog(region_name=config.catalog_name)
    for file in os.listdir(nll_results):
        if file.endswith("loc.hyp") and not file.endswith("sum.grid0.loc.hyp"):
            nll_results2 = os.path.join(nll_results, file)
            cat_temp = obspy.read_events(nll_results2, format='NLLOC_HYP')
            for event in cat_temp:
                for origin in event.origins:
                    origin.extra = {'algorithm': {'value': loc_algorithm,
                                                  'namespace': 'igf'}}
                cat.catalog.append(event)
    if only_p:
        cat.write(os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_p.xml"))
    else:
        cat.write(os.path.join(nll_path(date), f"{date.strftime('%d_%m_%Y')}_nll_ps.xml"))
