import pandas
import seisbench.models as sbm
import obspy
import datetime
import os

#from AutomaticLocationsTools.AutomaticDetector import year_and_day_info, year_and_day_to_string, detector_day_path
from .generate_paths import *
from AutomaticLocationsTools.AutomaticDetector import *
from AutomaticLocationsTools.NLL import run_nll, create_nll_input_files, prepare_nll_config, quakeml_from_nll, \
    xls_from_nll
import config
from AutomaticLocationsTools.det_to_qml import AutoCatalog, ppos

from obspy import UTCDateTime
from pyproj import CRS, Transformer
import pandas as pd
import numpy as np

from obspy import read_inventory

from gamma.utils import association


def return_stations(stream: obspy.Stream):
    list_of_stations = []
    for trace in stream:
        list_of_stations.append(trace.stats['station'])
    list_of_stations = list(set(list_of_stations))
    return list_of_stations


def seisbench_picker(stream: obspy.Stream):
    model = sbm.EQTransformer.from_pretrained('stead')
    list_of_stations = return_stations(stream)
    list_of_picks = []
    for i, station in enumerate(list_of_stations):
        print(f"Picking on station {station} - {i+1}/{len(list_of_stations)}")
        st_temp = stream.select(station=station)
        st_temp = st_temp.filter("bandpass", freqmin=config.freq_min, freqmax=config.freq_max)
        st_temp = st_temp.merge()
        picks, detections = model.classify(st_temp, P_threshold=config.p_wave_threshold,
                                           S_threshold=config.s_wave_threshold)
        for p in picks:
            list_of_picks.append(p)
        st_temp.clear()
    return list_of_picks


def gamma_detector(list_of_picks, date: UTCDateTime):
    """Function takes list of picks from SeisBench detector, and associate seismic phases using GaMMA software

    :param date: obspy UTCDateTime
    :param list_of_picks:
    :return:
    """
    # TODO: GaMMA parameters to external config file

    # detector parameters
    gamma_score = 0

    # Projections
    wgs84 = CRS.from_epsg(config.wgs84)
    local_crs = CRS.from_epsg(config.local_crs)  # CS POLAND 2000 zone5
    transformer = Transformer.from_crs(wgs84, local_crs)
    transformer2 = Transformer.from_crs(local_crs, wgs84)

    # Gamma
    config_g = dict()
    config_g["dims"] = ['x(km)', 'y(km)', 'z(km)']
    config_g["use_dbscan"] = True
    config_g["use_amplitude"] = False
    config_g["x(km)"] = config.x_limits
    config_g["y(km)"] = config.y_limits
    config_g["z(km)"] = config.z_limits
    config_g["vel"] = {"p": config.v_p, "s": config.v_s}
    config_g["method"] = "BGMM"
    if config_g["method"] == "BGMM":
        config_g["oversample_factor"] = 4
    if config_g["method"] == "GMM":
        config_g["oversample_factor"] = 1

    # DBSCAN
    config_g["bfgs_bounds"] = (
        (config_g["x(km)"][0] - 1, config_g["x(km)"][1] + 1),  # x
        (config_g["y(km)"][0] - 1, config_g["y(km)"][1] + 1),  # y
        (config_g["z(km)"][0], config_g["z(km)"][1]),  # z
        (None, None),  # t
    )
    config_g["dbscan_eps"] = config.config_g_min_picks_per_eq  # seconds
    config_g["dbscan_min_samples"] = config.config_g_dbscan_min_samples

    # Filtering
    config_g["min_picks_per_eq"] = config.config_g_min_picks_per_eq
    config_g["max_sigma11"] = config.config_g_max_sigma11
    config_g["max_sigma22"] = config.config_g_max_sigma22
    config_g["max_sigma12"] = config.config_g_max_sigma12

    # stations metadata
    inv = read_inventory(config.path_to_station_xml)
    inv = inv.select(time=date)
    station_df = []
    for station in inv[0]:
        station_df.append({
            "id": f"PL.{station.code}.",
            "longitude": station.longitude,
            "latitude": station.latitude,
            "elevation(m)": station.elevation
        })
    station_df = pd.DataFrame(station_df)
    station_df["x(km)"] = station_df.apply(lambda x: transformer.transform(x["latitude"], x["longitude"])[0] / 1e3,
                                           axis=1)
    station_df["y(km)"] = station_df.apply(lambda x: transformer.transform(x["latitude"], x["longitude"])[1] / 1e3,
                                           axis=1)
    station_df["z(km)"] = - station_df["elevation(m)"] / 1e3
    pick_df = []
    for p in list_of_picks:
        pick_df.append({
            "id": p.trace_id,
            "timestamp": p.peak_time.datetime,
            "prob": p.peak_value,
            "type": p.phase.lower()
        })
    pick_df = pd.DataFrame(pick_df)
    #pick_df = pick_df.loc[pick_df['prob'] > 0.3]
    catalogs, assignments = association(pick_df, station_df, config_g, method=config_g["method"])
    catalog = pd.DataFrame(catalogs)
    #catalog = catalog[catalog['gamma_score'] > gamma_score]
    if not catalog.empty:
        catalog = catalog.sort_values("time")
        catalog["lat"] = catalog.apply(lambda x: transformer2.transform(x["x(km)"] * 1000, x["y(km)"] * 1000)[0], axis=1)
        catalog["lon"] = catalog.apply(lambda x: transformer2.transform(x["x(km)"] * 1000, x["y(km)"] * 1000)[1], axis=1)
        catalog = catalog.reset_index(drop=True)
    #catalog.to_excel(os.path.join(directory_out, f"{date}_catalog.xlsx"))
    assignments = pd.DataFrame(assignments, columns=["pick_index", "event_index", "gamma_score"])
    #assignments.to_excel(os.path.join(directory_out, f"{date}_assignments.xlsx"))
    return catalog, assignments


def automatic_detector_sb(stream: obspy.Stream, date: UTCDateTime):
    picks = seisbench_picker(stream)
    print('Seismograms were picked')
    catalog, assignments = gamma_detector(picks, date)
    print('Events were detected')
    return picks, catalog, assignments


def quakeml_from_sb(picks, catalog, assignments, date: datetime):
    quakeml = save_quakeml(date=date, day_catalog_df=catalog, assignments=assignments, picks=picks)
    return quakeml


def run_nll_locations(quakeml, date: datetime):
    # locations only for P waves
    create_nll_input_files(quakeml, date, only_p=True)
    path = prepare_nll_config(date, only_p=True)
    run_nll(path)
    quakeml_from_nll(date, only_p=True)
    xls_from_nll(date, only_p=True)
    # for P and S
    create_nll_input_files(quakeml, date, only_p=False)
    path = prepare_nll_config(date, only_p=False)
    run_nll(path)
    quakeml_from_nll(date, only_p=False)
    xls_from_nll(date, only_p=False)


def create_trig_list_from_catalog(catalog: pd.DataFrame, assignments: pd.DataFrame, picks: list):
    trig_list = []
    for i, event in catalog.iterrows():
        trig = dict()
        trig['time'] = event["time"]
        event_picks = [picks[i] for i in assignments[assignments["event_index"] == event["event_index"]]["pick_index"]]
        list_of_stations = []
        list_of_times = []
        for pick in event_picks:
            if pick.phase == 'P':
                list_of_stations.append(pick.trace_id.split(".")[1])
                list_of_times.append(pick.peak_time.datetime)
        list_of_times = np.array(list_of_times)
        list_of_stations = np.array(list_of_stations)
        inds = list_of_times.argsort()
        list_of_times = list_of_times[inds]
        list_of_stations = list_of_stations[inds]
        trig['stations'] = list_of_stations
        trig['times'] = list_of_times
        trig['coincidence_sum'] = len(event_picks)
        trig_list.append(trig)
    return trig_list


def save_quakeml(date: datetime, day_catalog_df: pandas.DataFrame, assignments: pandas.DataFrame, picks):
    """ Function save quakeml catalog containing automaticly picked P (and optionally S phases)
    :param assignments:
    :param date:
    :param day_catalog_df:
    :param picks:
    :return:
    """

    cat = AutoCatalog(region_name=config.catalog_name)
    for j, row in day_catalog_df.iterrows():
        event = day_catalog_df.iloc[j]
        event_picks = [picks[i] for i in assignments[assignments["event_index"] == event["event_index"]]["pick_index"]]
        datetime_object = pd.to_datetime(event["time"])
        location = {'year': datetime_object.year, 'month': datetime_object.month,
                    'day': datetime_object.day,
                    'hour': datetime_object.hour, 'minute': datetime_object.minute,
                    'second': datetime_object.second, 'microsecond': datetime_object.microsecond,
                    'latitude': event.lat, 'longitude': event.lon, 'depth': 800.}
        pp = []
        for pick in event_picks:
            datetime_object2 = pick.peak_time.datetime
            network = pick.trace_id.split(".")[0]
            station = pick.trace_id.split(".")[1]
            channel = pick.trace_id.split(".")[2]
            pick_to_save = [network, station, channel, pick.phase, datetime_object2.year, datetime_object2.month,
                            datetime_object2.day, datetime_object2.hour, datetime_object2.minute,
                            datetime_object2.second, datetime_object2.microsecond]
            d = dict(zip(ppos, pick_to_save))
            if config.onlyP and pick.phase == 'P' or not config.onlyP:
                pp.append(d)
        cat.add_detection(pp, location, loc_algorithm="EQTransformer")
    output_file = os.path.join(detector_out_path(date), date.strftime("%d_%m_%Y_detection") + '.xml')
    cat.write(output_file)
    return output_file


