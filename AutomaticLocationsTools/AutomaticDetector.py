# Automatic Locations Flow: Automatic Detector
#
# Functions for automatic detections with Obspy coincidence trigger: gathering waveform data, processing waveform
# data, running detector, creating xlsx and txt outputs and saving mseed files with detected events together with
# BackTrackBB config files.
#
# Structure of output files:
# Detection_Results_Directory
#   - YYYY.NoD  where YYYY - year, NoD - number of day in the year (3 digits ex. 005)
#       - trig_n - event where n is a number of detected event starting from one
#            - lum_event.mseed - mseed seismic data
#            - lumineos.conf - BackTrackBB configuration file
#       - automatic_detector.txt - text file with summary of detected events
#   - YYYY.NoD_out
#
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from datetime import timedelta, datetime

import os
import os.path

import obspy
from obspy import read, Stream
from obspy.signal.trigger import coincidence_trigger
import pandas as pd

from .AutomaticDetectorSB import automatic_detector_sb, create_trig_list_from_catalog, quakeml_from_nll, \
    quakeml_from_sb, run_nll_locations
from .generate_paths import day_path, detector_out_path, detector_waveforms_out_path, btbb_out_path, \
    btbb_relocation_out_path, create_dir_if_not_exist
# importing configuration file, which should be edited by users
import config


def check_if_detections_exist(date: datetime) -> bool:
    """Function checks if directory with detections exists and is not empty.

    :param date: datetime
    :return: False if directory does not exist or is empty
    """

    flag = False
    path = day_path(date)
    if os.path.exists(path) and os.path.isdir(path):
        if os.listdir(path):
            flag = True
    return flag


def date_range(start: datetime, end: datetime):
    """Function returns range of dates.

    :param start: datetime
    :param end: datetime
    :return: range of datetime objects
    """
    end += timedelta(hours=1)
    dates_range = start
    while dates_range < end:
        yield dates_range
        dates_range += timedelta(days=1)
    return dates_range


def replace_year_in_path(year: int) -> str:
    """Function place number of given year in a waveform path.

    :param year: int
    :return: str
    """

    return config.path_to_waveforms.replace('XXXX', str(year))


def waveform_file_name(station: str, channel: str, date_str: str) -> str:
    """Function returns name of waveform file.

    :param station: str
    :param channel: str
    :param date_str: str
    :return: str
    """

    file = 'PL.' + station + '..' + channel + '.' + date_str + '*'
    return file


def obtain_data_for_detector(date_str: str) -> obspy.Stream:
    """Function creates ObsPy Stream containing traces from all requested stations.

    :param date_str: str, ex. 2021.012
    :return: obspy.Stream
    """

    st = Stream()
    for station in config.stations:
        path_to_station = os.path.join(config.path_to_waveforms, station)
        if os.path.exists(path_to_station):

            channels = [x[1] for x in os.walk(path_to_station)][0]
            list_of_channels=[]
            for channel in channels:
                if channel[2] not in list_of_channels:
                    file = waveform_file_name(station=station, channel=channel, date_str=date_str)
                    path = os.path.join(path_to_station, channel, file)
                    try:
                        tmp = read(path)
                        if tmp[0].stats['sampling_rate'] == config.accel_freq:
                            # changing sampling frequency of accelerometers
                            tmp.interpolate(sampling_rate=config.seism_freq)
                        st += tmp
                        print("Loaded: ", station, channel)
                        list_of_channels.append(channel[2])
                    except Exception:
                        print("No: ", station, channel)
                        continue
                else:
                    print("Channel ", channel[2], " of station ", station, " already exist")
    print("Data was prepared")
    return st


def obtain_times_for_btbb_relocation(path: str, trig_nr: int):
    """Function reads events time from detection and location results and creates time window for BTBB relocation.

    :param path: str
    :param trig_nr: int
    :return:
    """
    catalog = pd.read_excel(os.path.join(path, config.final_xlsx_file), index_col='Nr')
    time_btbb=catalog['Date'][int(trig_nr)]
    time_obspy=catalog['Date_detector'][int(trig_nr)]
    time_diff = (time_btbb - time_obspy).total_seconds() + config.time_before_detection
    time_start = round(time_diff-config.time_before_first_location, 2)
    time_end = round(time_start + config.window_end, 2)
    return time_start, time_end


def yesterday():
    """Function returns yesterday datetime object.

    :return: datetime
    """

    today = datetime.now()
    return today - timedelta(days=1)


def year_and_day_info(date: datetime) -> (int, int):
    """ Function returns number of year and number of day in the year.

    :param date: datetime
    :return: (int, int)
    """

    return date.timetuple().tm_year, date.timetuple().tm_yday


def year_and_day_to_string(year: int, day_of_year: int) -> str:
    """Function returns string 'YYYY.DoY' (ex. 2021.012) where YYYY is number of the year, and DoY is number of
    day in the year (3 digits).

    :param year: int
    :param day_of_year: int
    :return: str
    """

    return str(year) + '.' + str(f"{day_of_year:03d}")


def remove_stations_with_gaps(stream: obspy.Stream) -> obspy.Stream:
    """Function removes traces with gaps from the obspy stream object.

    :param stream: obspy.Stream
    :return: obspy.Stream
    """

    gaps = stream.get_gaps()
    st_with_gaps = []
    for gap in gaps:
        st_with_gaps.append(gap[1])
    if st_with_gaps:
        print("Stations with gaps - not saved: ", st_with_gaps)
    for station in st_with_gaps:
        for tr in stream.select(station=station):
            stream.remove(tr)
    stream.merge()
    return stream


def create_detector_xls_output_file(path_out: str, trig_list: list):
    """Function creates xlsx file containing detection results

    File contains: trig number, date, number of
    traces (coincidence sum of ObsPy coincidence trigger) and number of stations which exceeded given STA/LTA ratio

    :param path_out:
    :param trig_list:
    :return:
    """

    lst_events = []
    j = 1
    for trig in trig_list:
        number_of_stations = len(set(trig['stations']))
        lst_events.append([j, trig['time'], trig['coincidence_sum'], number_of_stations])
        j += 1
    df = pd.DataFrame(lst_events, columns=['Nr', 'Date', 'Traces', 'Stations'])
    df.to_excel(os.path.join(path_out, config.detector_xlsx_file))


# TODO: change saved variables according to detector (EQTransformer - number of stations, number of picks)
def create_detector_txt_output_file(date: datetime, path_out: str, trig_list: list):
    """Function creates output text file containing all detected events and information about them:
    list of stations, date, number of traces which exceeded STA/LTA ratio.

    :param date:
    :param path_out:
    :param trig_list:
    :return:
    """

    filename = date.strftime('%d_%m_%Y')+'.txt'
    with open(os.path.join(path_out, filename), 'w') as f:
        f.write('Automatic detector')
        f.write('\n')
        f.write(filename)
        f.write('\n')
        f.write(f"Detector type: {config.detector_type}")
        f.write('\n')
        f.write("#" * 80)
        f.write('\n')
        f.write('Number of detected events: '+str(len(trig_list)))
        f.write('\n')
        for trig in trig_list:
            t = trig['time']
            f.write("#" * 80)
            f.write('\n')
            f.write("Trigger time:")
            f.write(str(t))
            f.write('\n')
            f.write("The closest station:")
            if len(trig['stations']) > 0:
                f.write(str(trig['stations'][0]))
            f.write('\n')
            all_stations = set(trig['stations'])
            f.write("Number of stations: " + str(len(all_stations)))
            f.write('\n')
            f.write("All stations: ")
            f.write(str(all_stations))  # only unique values
            f.write('\n')
            c = trig['coincidence_sum']
            f.write("Coincidence sum:")
            f.write(str(c))
            f.write('\n')


def prepare_btbb_conf_file(event: str, date: datetime, version='long', nr='2'):
    """ Function prepares BackTrackBB configuration files for given event.

    :param event : str
        The name of directory containing detected event mseed file and BTBB conf files: 'trig_NR', ex. 'trig_1'
    :param date: str - path to directory with detections from one day ex. to 2021.012
    :param version: str label - 'long' (location BTBB file) or 'short' (relocation BTBB file)
    :param nr: str label of the output directory - ex. 2021.012_out2 - typically for location
    it is None and for relocation is equal 2
    :return:
    """
    path = detector_waveforms_out_path(date)
    path_btbb = btbb_out_path(date)
    path_btbb_rel = btbb_relocation_out_path(date, nr)
    with open(config.BTBB_conf_file_base, 'r') as f:
        trig_nr = int(event.split('_')[-1])
        save_path = os.path.join(path, event)
        if version == 'long':
            complete_name = os.path.join(save_path, config.BTBB_conf_file_long)
        elif version == 'short':
            complete_name = os.path.join(save_path, config.BTBB_conf_file_short)
            start_time, end_time = obtain_times_for_btbb_relocation(path_btbb, trig_nr)
        file_out = open(complete_name, "w")
        lines = f.readlines()
        for i, line in enumerate(lines):
            if version == 'long':
                if 'data_dir' in line:
                    text = './'+save_path
                    line = f'data_dir = "{text}"\n'
                if 'out_dir' in line:
                    text = './'+os.path.join(path_btbb, event)
                    line = f'out_dir = "{text}"\n'
            if version == 'short':
                if 'data_dir' in line:
                    text = './'+save_path
                    line = f'data_dir = "{text}"\n'
                if 'out_dir' in line:
                    text = './'+os.path.join(path_btbb_rel, event)
                    line = f'out_dir = "{text}"\n'
                if 'time_lag' in line:
                    line = "time_lag = "+str(config.time_window)+'\n'
                if 'start_t' in line:
                    line = "start_t = "+str(start_time)+'\n'
                if 'end_t' in line:
                    line = "end_t = "+str(end_time)+'\n'
            file_out.write(line)
        file_out.close()


def prepare_list_of_day_events(path: str):
    """ Function prepare list of events from a given day (path)

    :param path: str - path to directory containing detected events ex. 2021.012
    :return: os.walk(path) object - list of directories in a path
    """

    return os.walk(path)


def create_relocation_btbb_configs(date: datetime, nr='2'):
    """Function creates BackTrackBB config file for relocation for a given date and given label nr

    :param date: datetime object
    :param nr: str - label for output directories
    :return:
    """

    catalog = pd.read_excel(os.path.join(btbb_out_path(date), config.final_xlsx_file))
    events = ['trig_'+str(x) for x in catalog['Nr']]
    for name in events:
        prepare_btbb_conf_file(name, date, version='short', nr=nr)


def save_detected_events(st: obspy.Stream, trig_list: list, date: datetime):
    """Function save detected events to separate mseed files as well as call function which prepares
     BackTrackBB config files

    :param st: ObsPy stream object
    :param trig_list: list of triggered events - output of ObsPy coincidence trigger
    :param date: datetime
    :return:
    """
    path = detector_waveforms_out_path(date)
    if trig_list:
        j = 0
        for trig in trig_list:
            t = obspy.UTCDateTime(trig['time'])
            j += 1
            event = 'trig_' + str(j)
            event_path = os.path.join(path, event)
            create_dir_if_not_exist(event_path)
            delta1 = timedelta(seconds=config.time_before_detection)
            delta2 = timedelta(seconds=config.time_after_detection)
            st2 = st.copy()
            # trim time
            st2.trim(starttime=t - delta1, endtime=t + delta2)
            # remove stations with gaps
            st2 = remove_stations_with_gaps(st2)
            # write mseed files
            st2.write(os.path.join(event_path, config.mseed_name), format="MSEED")
            st2.clear()
            prepare_btbb_conf_file(event, date)


# TODO: put coincidence parameters into config file
def automatic_detection(date: datetime, detector=config.detector_type, detection_output=True, btbb_files=True,
                        only_z_comp=True, nll_locations=True):
    """Function performs automatic detection. It reads waveforms and use ObsPy coincidence trigger to detect events,
    save them to seperate mseed files and creates BackTrackBB config files as well as save detection results to xlsx
    and txt files.

    :param nll_locations: bool
    :param detector: str, type of detector: 'EQTransformer' or 'CoincidenceTrigger'
    :param date: datetime object
    :param detection_output: Boolean - xlsx and txt files containing information about detected events
    :param btbb_files: Boolean - mseed files with seismograms of detected events and config files for BackTrackBB
    :param only_z_comp: Boolean - if True only Z component is saved to files
    :return:
    """

    print("##########################################")
    print("Start of automatic detection")
    print(date.strftime('%d-%m-%Y'))
    year, day_of_year = year_and_day_info(date)
    date_string = year_and_day_to_string(year, day_of_year)
    config.path_to_waveforms = replace_year_in_path(year)
    # Obtain data from the server or local computer - you can create your own function for this purpose.
    # It has to return ObsPy Stream object
    st = obtain_data_for_detector(date_string)
    trig_list = []
    if st:
        if detector == 'CoincidenceTrigger':
            # Filtration
            st = st.filter("bandpass", freqmin=config.freq_min, freqmax=config.freq_max)
            print("Data filtered")
            # Detections
            trig_list = coincidence_trigger("recstalta", 10, 2, st, config.coinc_trigger, sta=0.5, lta=10)
            # Saving txt file with detections results
            if detection_output and trig_list:
                create_detector_txt_output_file(date, detector_out_path(date), trig_list)
                create_detector_xls_output_file(detector_out_path(date), trig_list)
        elif detector == 'EQTransformer':
            picks, catalog, assignments = automatic_detector_sb(st, obspy.UTCDateTime(date))
            quakeml = quakeml_from_sb(picks, catalog, assignments, date)
            trig_list = create_trig_list_from_catalog(catalog, assignments, picks)
            # Saving txt file with detections results
            if detection_output and trig_list:
                create_detector_txt_output_file(date, detector_out_path(date), trig_list)
                create_detector_xls_output_file(detector_out_path(date), trig_list)
            if nll_locations:
                run_nll_locations(quakeml, date)

        else:
            print(f"No detections- wrong detector parameter: {detector}")
        print("Detection finished")



        # save events to separate files
        if btbb_files and trig_list:
            # choose only Z channels (BTBB needs only Z channels)
            if only_z_comp:
                st = st.select(component='*Z')
            save_detected_events(st, trig_list, date)
        st.clear()
    else:
        print("Stream is empty")

    print("End of automatic detection")
    print("##########################################")

#######################################################


if __name__ == "__main__":
    date = datetime(year=2021, month=1, day=1)
    automatic_detection(date, btbb_files=False)
