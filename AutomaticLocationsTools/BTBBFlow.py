#  Automatic Locations Flow: BTBB Flow
#
#  Functions for running BackTrackBB software and for creating xlsx and quakeML output files
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from datetime import datetime, timedelta
import os

import pandas as pd
from typing import Tuple, List

from AutomaticLocationsTools.AutomaticDetector import year_and_day_info, year_and_day_to_string, prepare_list_of_day_events
from AutomaticLocationsTools.generate_paths import btbb_relocation_out_path, detector_out_path, detector_waveforms_out_path, btbb_out_path
from AutomaticLocationsTools.det_to_qml import AutoCatalog, ppos
# importing configuration file, which should be edited by users
import config


def return_list_of_not_located_events(date: datetime, relocation=False, nr=''):
    """Return paths to not located events.

    :param date:
    :param relocation:
    :param nr:
    :return: list of paths to (detection) directories of not located events
    """

    path = detector_waveforms_out_path(date)
    detector_list = os.walk(path)
    if relocation:
        path_loc = btbb_relocation_out_path(date, nr=nr)
    else:
        path_loc = btbb_out_path(date)
    location_list = os.walk(path_loc)
    detected_events = [r[1:] for r in detector_list][0][0]
    located_events = [r[1:] for r in location_list][0][0]
    not_located_events = []
    for event in detected_events:
        if event not in located_events:
            event = os.path.join(path, event)
            not_located_events.append(event)
    not_located_events2 = []
    for event in located_events:
        if not os.listdir(os.path.join(path_loc,event)):
            event = os.path.join(path, event)
            not_located_events2.append(event)
    return not_located_events+not_located_events2


def run_btbb(btbb_conf_file_path: str):
    """Function runs BTBB software using a path to a BTBB configuration file.

    :param btbb_conf_file_path: str
    :return:
    """

    print("##########################################")
    print("start of BTBB")
    os.system('btbb ' + btbb_conf_file_path)
    print("end of BTBB")
    print("##########################################")


def run_btbb_for_list_of_events(list_of_events: list, btbb_config_file: str):
    """Function runs BTBB for all directories in the list_of_events.

    :param list_of_events:
    :param btbb_config_file:
    :return:
    """

    for event in list_of_events:
        btbb_conf_path = os.path.join(event, btbb_config_file)
        if os.path.exists(btbb_conf_path):
            print(btbb_conf_path)
            run_btbb(btbb_conf_file_path=btbb_conf_path)
        else:
            print(btbb_conf_path, ' does not exist')


def btbb_automatic_locations(date: datetime, btbb=True, quakeml=True, relocation=False, catalogs=True, nr=''):
    """Main function which can be used for running BackTrackBB software for all triggered events.

    Main function which can be used for running BackTrackBB software for all triggered events for a given day
    (btbb option) or/and saving quakeML files (quakeML option) or/and saving one day xlsx output files.
    Function can be used for location or relocation.

    Function checks which events were already located (in a given output path) and is not calculating locations once again.

    BackTrackBB software needs to be previously installed (https://github.com/BackTrackBB/backtrackbb)

    :param date: datetime
    :param btbb: bool, True for running locations
    :param quakeml: bool, True for creating quakeml output files
    :param relocation: bool, True for shorter time windows during location
    :param catalogs: bool, True for creating output xlsx files
    :param nr: str, label for directory names
    :return:
    """

    print("##########################################")
    print("Start of automatic location")
    print(date.strftime('%d-%m-%Y'))

    list_of_events = return_list_of_not_located_events(date=date, relocation=relocation, nr=nr)
    if list_of_events:
        if btbb:
            print("##########################################")
            if relocation:
                run_btbb_for_list_of_events(list_of_events=list_of_events, btbb_config_file=config.BTBB_conf_file_short)
            else:
                run_btbb_for_list_of_events(list_of_events=list_of_events, btbb_config_file=config.BTBB_conf_file_long)
    print("End of automatic location")
    if quakeml:
        print('Save quakeml:')
        if relocation:
            list_of_events = prepare_list_of_day_events(btbb_relocation_out_path(date, nr=nr))
        else:
            list_of_events = prepare_list_of_day_events(btbb_out_path(date))
        save_quakeml(date, list_of_events, nr=nr)
    if catalogs:
        generate_one_day_xls_catalog(date, nr=nr)
        join_detection_location_outputs(date, nr=nr)
    print("##########################################")


def check_if_btbb_file_exist_and_isnot_empty(event_path: str) -> bool:
    """Function checks if BackTrackBB output dat file exist and is not empty.

    :param event_path: str
    :return: bool
    """

    flag = False
    file = return_path_to_btbb_dat_file(event_path)
    if file:
        with open(file, 'r') as f:
            text = f.readlines()
            if text:
                flag = True
    return flag


def return_path_to_btbb_dat_file(event_path: str) -> str:
    """Function returns path to output BackTrackBB .dat file.

    :param event_path: str
    :return: str
    """

    for file in os.listdir(event_path):
        if file.endswith(".dat"):
            return os.path.join(event_path, file)


def read_btbb_output(event_path: str) -> Tuple[str, str, str, str, str]:
    """Returns location information and statistics from BackTrackBB output file.

    :param event_path:
    :return: Tuple[str, str, str, str, str] origin time, latitude, longitude, RMS, MaxSt
    """

    with open(return_path_to_btbb_dat_file(event_path), 'r') as f:
        line=f.readline()
        time, lat, lon, rms, maxst = location_from_btbb_output(line=line)
    return time, lat, lon, rms, maxst


def read_btbb_output_full(event_path: str) -> Tuple[dict, List[list]]:
    """Returns location and picks from BackTrackBB output file for quakeML

    location: year, month, day, hour, minute, second, microsecond, latitude, longitude, depth

    :param event_path:
    :return: Tuple[dict, List[list]]
    """

    pp = []
    with open(return_path_to_btbb_dat_file(event_path), 'r') as f:
        for line in f:
            if 'MaxStack' in line:
                time, lat, lon, rms, maxst = location_from_btbb_output(line=line)
                datetime_object = datetime.strptime(time, '%Y-%m-%dT%H:%M:%S.%fZ')
                location = {'year': datetime_object.year, 'month': datetime_object.month,
                            'day': datetime_object.day,
                            'hour': datetime_object.hour, 'minute': datetime_object.minute,
                            'second': datetime_object.second, 'microsecond': datetime_object.microsecond,
                            'latitude': float(lat), 'longitude': float(lon), 'depth': config.const_depth}
            elif 'sta' in line:
                index1 = line.find("sta")
                index2 = line.find("Ph")
                station = line[index1 + 4:index2 - 2]
                if station in config.seismometers:
                    channel = 'EHZ'
                elif station in config.accelerometers:
                    channel = 'CNZ'
                else:
                    print('WARNING! ', station, ' is not in the list!')
                index4 = line.find("TT")
                th_time = line[index4 + 3:index4 + 7]
                index3 = line.find("PT")
                pick_time = line[index3 + 3:index3 + 7]
                delta = timedelta(
                    seconds=float(pick_time))
                datetime_object2 = datetime_object + delta
                if abs(float(th_time) - float(pick_time)) < config.time_difference:
                    #TODO: replace PL
                    pick_to_save = ['PL', station, channel, 'P', datetime_object2.year, datetime_object2.month,
                                    datetime_object2.day, datetime_object2.hour, datetime_object2.minute,
                                    datetime_object2.second, datetime_object2.microsecond]
                    pp.append(pick_to_save)
        return location, pp


def create_dataframe_from_list_of_located_events(list_of_located_events) -> pd.DataFrame:
    """Function returns dataframe with one day location results from BackTrackBB taking as an input os.walk.path object
    containing paths to events (trig* directories).

    :param list_of_located_events: os.walk.path object containing root, dirs and files
    :return: pd.DataFrame
    """

    trig_nrs = []
    times = []
    lats = []
    lons = []
    rmss = []
    maxsts = []
    for root, dirs, files in list_of_located_events:
        for name in dirs:
            event_path = os.path.join(root, name)
            if check_if_btbb_file_exist_and_isnot_empty(event_path):
                trig_nrs.append(str(event_path).split('_')[-1])
                time, lat, lon, rms, maxst = read_btbb_output(event_path)
                times.append(time)
                lats.append(lat)
                lons.append(lon)
                rmss.append(rms)
                maxsts.append(maxst)
    if times and times[0]:
        d = {'Nr': trig_nrs, 'Date': times, 'Lat': lats, 'Lon': lons, 'RMS': rmss, 'MaxStack': maxsts}
        df = pd.DataFrame(data=d)
        return df


def generate_one_day_xls_catalog(date: datetime, nr=''):
    """Function saves xlsx file containing location statistic from one day of BackTrackBB outputs.

    :param date: datetime
    :param nr: str label
    :return:
    """
    if nr == '':
        day_path_btbb = btbb_out_path(date)
    else:
        day_path_btbb = btbb_relocation_out_path(date, nr=nr)
    list_of_located_events = prepare_list_of_day_events(day_path_btbb)
    df = create_dataframe_from_list_of_located_events(list_of_located_events)
    df.to_excel(os.path.join(day_path_btbb, config.BTBB_xlsx_file))


def join_detection_location_outputs(date: datetime, nr=''):
    """Function creates and saves final xlsx file containing joined detection and location parameters

    :param date: datetime
    :param nr: str label
    :return:
    """

    day_path = detector_out_path(date)

    if nr == '':
        day_path_btbb = btbb_out_path(date)
    else:
        day_path_btbb = btbb_relocation_out_path(date, nr=nr)

    df_detected = pd.read_excel(os.path.join(day_path,config.detector_xlsx_file))
    df_located = pd.read_excel(os.path.join(day_path_btbb,config.BTBB_xlsx_file))

    df_located['Date'] = pd.to_datetime(df_located['Date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
    try:
        df_detected['Date_detector'] = pd.to_datetime(df_detected['Date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
    except ValueError:
        df_detected['Date_detector'] = pd.to_datetime(df_detected['Date'], format='%Y-%m-%dT%H:%M:%S.%f')
    df_detected.drop(['Date'], axis=1, inplace=True)
    df_output = pd.merge(df_located, df_detected, how="inner", left_on='Nr', right_on='Nr')
    df_output.drop(['Unnamed: 0_x','Unnamed: 0_y'], axis=1, inplace=True)
    df_output.sort_values(by=['Nr'], inplace=True)
    df_output.set_index('Nr', inplace=True)
    df_output.to_excel(os.path.join(day_path_btbb,config.final_xlsx_file))


def location_from_btbb_output(line: str) -> Tuple[str, str, str, str, str]:
    """Function takes as an argument first line of BackTrackBB output file and return location statistics.

    :param line: str
    :return: Tuple[str, str, str, str, str] origin time, latitude, longitude, rms, maxst
    """

    index = line.find("LAT")
    index2 = line.find("LON")
    index3 = line.find("T_ORIG")
    index4 = line.find("RMS-P")
    index5 = line.find("MaxStack")
    if index != -1:
        lat = line[index + 4:index2 - 1]
        rms = line[index4 + 6:index4 + 10]
        maxst = line[index5 + 9:index5 + 14]
        lon = line[index2 + 4:index3 - 1]
        time = line[index3 + 7:index4 - 1]
        return time, lat, lon, rms, maxst
    else:
        print("No data in BTBB file")


def save_quakeml(date: datetime, list_of_events, nr=''):
    """Function creates AutoCatalog object (catalog object from ObsPy) and save it to location or relocation path
    (_out or _out2 depending on nr variable)

    :param date: datetime
    :param list_of_events: os.walk object
    :param nr: str label
    :return:
    """

    cat = AutoCatalog(region_name=config.catalog_name)

    for root, dirs, files in list_of_events:
        for name in dirs:
            event_path = os.path.join(root, name)
            if check_if_btbb_file_exist_and_isnot_empty(event_path=event_path):
                location, pp = read_btbb_output_full(event_path=event_path)
                picks = list()
                for p in pp:
                    d = dict(zip(ppos, p))
                    picks.append(d)
                cat.add_detection(your_picks=picks, your_location=location, loc_algorithm="BTBB")
            else:
                print('No location in file:', event_path)
    year, day = year_and_day_info(date=date)
    day_string = year_and_day_to_string(year=year, day_of_year=day).replace('.', '_')
    if nr == '':
        output_file = os.path.join(btbb_out_path(date), f"{date.strftime('%d_%m_%Y')}_btbb.xml")
    else:
        output_file = os.path.join(btbb_relocation_out_path(date, nr=nr), f"{date.strftime('%d_%m_%Y')}_btbb.xml")
    print("Output file:\n   {}".format(output_file))
    cat.write(output_file)


if __name__ == "__main__":
    os.chdir('..')
    date = datetime(year=2021, month=1, day=1)
    return_list_of_not_located_events(date=date)
