# Automatic detections
#
# Configuration file
#
#  Copyright (C) 2022  Jakub Kokowski
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# Path to waveforms on Tytan server
#path_to_waveforms = '/home/sysop/seiscomp3/var/lib/archive/XXXX/PL/'  # XXXX has to be replaced by year
path_to_waveforms = './archive2/XXXX/PL/'
###########################################
# Paths
# output directory path
path_out = "Detection_Results_nll"
###########################################
# Name of files
# Base name of BackTrackBB configuration file. This file should be in the main catalog where the software is started.
BTBB_conf_file_base = 'BTBB.conf'
# Name of the first saved BackTrackBB conf file
BTBB_conf_file_long = 'lumineos_long.conf'
# Name of the NonLinLoc config file
NLL_conf_file = 'nlloc_config.in'
# Names of saved mseed event files
mseed_name = 'lum_event.mseed'
# Names of xlsx files containing location and detection results
BTBB_xlsx_file = 'automatic_location_output.xlsx'
detector_xlsx_file = 'automatic_detection_output.xlsx'
final_xlsx_file = 'btbb_catalog.xlsx'
###########################################
# Detector type: 'EQTransformer' or 'CoincidenceTrigger'
detector_type = 'EQTransformer'
###########################################
# Automatic detector parameters - Coincidence trigger
coinc_trigger = 7  # minimal number of traces with STA/LTA alert to save the event
###########################################
# Automatic detector parameters - EQTransformer
p_wave_threshold = 0.1
s_wave_threshold = 0.1
onlyP = True # saving only P picks for quakeml file
###########################################
# Automatic detector parameters - Gamma
path_to_station_xml = 'LUMINEOS_Stations_all.xml'
# coordinates
wgs84 = 4326
local_crs = 2176  # CS POLAND 2000 zone5
x_limits = (5690, 5720)
y_limits = (5560, 5600)
z_limits = (0, 4)
###########################################
# NonLinLoc parameters
NLL_inputs_path = 'NLL_inputs'  # directory in detection results
NLL_outputs_path = 'NLL_outputs'  # directory in detection results
NLL_config_template = 'lumineos_NLL'
###########################################
# Seismic stations
seismometers = ['BRDW', 'DWOL', 'GROD', 'JEDR', 'KWLC', 'LUBW', 'LUBZ', 'MOSK2', 'NWLU', 'PPOL', 'RUDN', 'RYNR',
                'SGOR', 'TRN2', 'TRZS', 'ZMST', 'ZUKW2']
accelerometers = ['DABR', 'GUZI', 'KAZI', 'KOMR', 'KRZY', 'PCHB', 'PEKW2', 'RZEC', 'TRBC2', 'OBIS']
stations = seismometers+accelerometers
###########################################
# Frequencies of signal - used for downsampling the data to lower frequency
seism_freq = 100
accel_freq = 250
###########################################
# Frequencies of a bandpass filter
freq_min = 2
freq_max = 30
###########################################
# BTBB input data parameters
time_before_detection = 7  # seconds
time_after_detection = 13  # seconds
###########################################
# BTBB input data parameters for relocation
# Name of the next (relocation) BackTrackBB conf file
BTBB_conf_file_short = 'lumineos_short.conf'
time_window = 5.8
time_before_first_location = 0.3

window_end = time_window-time_before_first_location
###########################################
# BTBB quakeml paramters
time_difference = 0.3  # if time difference is smaller pick will be saved in quakeML file
catalog_name = 'LUMINEOS'
const_depth = 800  # constant depth in meters